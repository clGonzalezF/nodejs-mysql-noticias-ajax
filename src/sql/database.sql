/* 
powershell XAMP
mysql -u root -p
pegar contenido >
*/

CREATE DATABASE news_portal;

USE news_portal;

CREATE TABLE news(
    id_news INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(100),
    content TEXT,
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP
);

DESCRIBE news;

INSERT INTO news(title, content) values('Titulo de prueba', 'contenido de prueba de la noticia');

SELECT * FROM news;