const dbConnection = require('../../config/dbConnection');
const dateFormat = require('dateformat');

function allDateFormat(e){
    if(Array.isArray(e)){
        for(let i in e){
            e[i].date_created = dateFormat(e[i].date_created, "dd/mm/yyyy h:MM:ss");
        }
        return e;
    }else{
        return dateFormat(e.date_created, "dd/mm/yyyy h:MM:ss");
    }
}

module.exports = app => {
    const connection = dbConnection();

    app.get('/', (req, res) => {
        connection.query('SELECT * FROM news', (error, result) =>{
            res.render('news/news', {
                data: result,
                dateFormat: dateFormat
            });
        });
        //res.send('hola mundo');
    });

    app.post('/get_news', (req, res) => {
        connection.query('SELECT * FROM news', (error, result) =>{
            result = allDateFormat(result);
            res.send({
                status: true,
                data: result
            });
        });
    });

    app.post('/news', (req, res) => {
        const data = req.body;
        const tmp_error = [];

        if(typeof data.title == 'undefined' && data.title == '' && data.title == null){
            tmp_error.push('Title no definido');
        }
        if(typeof data.content == 'undefined' && data.content == '' && data.content == null){
            tmp_error.push('Content no definido');
        }

        connection.query('INSERT INTO news SET?', {
            title: data.title,
            content: data.content
        }, (error, result) => {
            if(result){
                connection.query('SELECT * FROM news ORDER BY id_news DESC LIMIT 1', (error, result) =>{
                    result[0].date_created = dateFormat(result[0].date_created, "dd/mm/yyyy h:MM:ss");
                    res.send({
                        status: true,
                        data: result[0], 
                        error: tmp_error 
                    });
                });
            }else if(error){
                res.send({
                    status: false,
                    error: tmp_error 
                });
            }
        });
    });
}