const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();

// settings
app.set('port', process.env.PORT || 8080); //puerto de server o 3000
//app.set('view engine', 'ejs');//motor de vista
//app.set('views', path.join(__dirname, '../app/views'));//ruta de vistas

app.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'../../index.html'));
  });

// middleware
app.use(bodyParser.urlencoded({ extended: false }));

module.exports = app;